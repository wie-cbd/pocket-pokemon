import React from "react";
import { render } from "@testing-library/react";
import { BrowserRouter, Router } from "react-router-dom";
import { createMemoryHistory } from "history";
import Container from "./Container";

test("renders the pokemon list page", () => {
  const { getByTestId } = render(
    <BrowserRouter>
      <Container />
    </BrowserRouter>
  );
  const element = getByTestId("pokemon-list-page");
  expect(element).toBeInTheDocument();
});

test("renders the pokedex page", () => {
  const history = createMemoryHistory();
  history.push("/pokedex/");
  const { getByTestId } = render(
    <Router history={history}>
      <Container />
    </Router>
  );
  const element = getByTestId("pokedex-page");
  expect(element).toBeInTheDocument();
});

test("renders the pokemon details page", () => {
  const history = createMemoryHistory();
  history.push("/detail/1");
  const { getByTestId } = render(
    <Router history={history}>
      <Container />
    </Router>
  );
  const element = getByTestId("pokemon-details-page");
  expect(element).toBeInTheDocument();
});

test("renders the pokemon list page on bad route", () => {
  const history = createMemoryHistory();
  history.push("/boohoo/hoo/1");
  const { getByTestId } = render(
    <Router history={history}>
      <Container />
    </Router>
  );
  const element = getByTestId("pokemon-list-page");
  expect(element).toBeInTheDocument();
});
