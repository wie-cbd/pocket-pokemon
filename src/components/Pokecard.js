import React from "react";
import { Link } from "react-router-dom";
import { removePokemon } from "../services/PokeAPI";

export const Pokecard = ({ name, id, index, mine }) => {
  return (
    <div className="poke-card">
      <Link to={`/detail/${id}`}>
        <img
          alt={`sprite-${name}`}
          src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`}
        />
        <h3 className="capitalize">{name}</h3>
      </Link>
    </div>
  );
};

export default Pokecard;
