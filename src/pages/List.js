import React from "react";
import { withRouter, Link } from "react-router-dom";
import { getPaginatedPokemons } from "../services/PokeAPI";
import Pokecard from "../components/Pokecard";

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pokemons: [],
      pageNumber:
        this.props.match.params.number !== undefined
          ? parseInt(this.props.match.params.number)
          : 0,
      prevPage: 0,
      nextPage: 0
    };
  }
  componentDidMount() {
    this.loadPokemons(this.state.pageNumber);
  }
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
      if (this.props.match.params.number !== undefined) {
        const page = parseInt(this.props.match.params.number);
        this.setState({ pageNumber: page, pokemons: [] });
        this.loadPokemons(page);
      } else {
        this.setState({ pageNumber: 0, pokemons: [] });
        this.loadPokemons(0);
      }
    }
  }
  async loadPokemons(number) {
    getPaginatedPokemons(number, 24).then(result => {
      this.setState({ pokemons: result });
    });
  }
  render() {
    return (
      <div data-testid="pokemon-list-page">
        <div className="page-title">
          <h1 data-testid="list-page-title">Pokemons</h1>
        </div>
        <div className="pokecard-container">
          {this.state.pokemons.length > 0 ? (
            <>
              {this.state.pokemons.map((poke, index) => {
                const pokeId = poke.url
                  .substring(0, poke.url.length - 1)
                  .replace("https://pokeapi.co/api/v2/pokemon/", "");
                return (
                  <Pokecard
                    key={`poke-${index}`}
                    name={poke.name}
                    id={pokeId}
                  />
                );
              })}
            </>
          ) : (
            <i>loading...</i>
          )}
        </div>
        <div className="pagination">
          {this.state.pageNumber - 1 >= 0 ? (
            <Link
              className="pagination-link"
              to={`/list/${this.state.pageNumber - 1}`}
            >
              Previous
            </Link>
          ) : (
            ""
          )}
          <Link
            className="pagination-link"
            to={`/list/${this.state.pageNumber + 1}`}
          >
            Next
          </Link>
        </div>
      </div>
    );
  }
}

export default withRouter(List);
