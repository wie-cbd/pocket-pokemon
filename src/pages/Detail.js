import React from "react";
import { withRouter } from "react-router-dom";
import { getPokemonById, addPokemon } from "../services/PokeAPI";
import { Link } from "react-router-dom";

class Detail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pokemon: null,
      pokemonId: null,
      caught: false,
      tried: false,
      added: false,
      nickname: ""
    };
    this.handleNicknameInput = this.handleNicknameInput.bind(this);
    this.handleNicknameSubmit = this.handleNicknameSubmit.bind(this);
  }
  componentDidMount() {
    const param = this.props.match.params.pokeId;
    this.setState({ pokemonId: param });
    if (param !== null && param !== undefined) {
      this.loadPokemon(param);
    }
  }
  async loadPokemon(id) {
    getPokemonById(id).then(res => {
      this.setState({ pokemon: res });
    });
  }
  handleNicknameInput(event) {
    this.setState({ nickname: event.target.value });
  }
  handleNicknameSubmit(event) {
    let { pokemon, pokemonId, nickname } = this.state;
    addPokemon(pokemonId, pokemon.name, nickname);
    this.setState({ added: true });
    event.preventDefault();
  }
  tryToCatch() {
    const caught = Math.random() < 0.5;
    this.setState({ tried: true });
    if (caught) {
      this.setState({ caught: caught });
    }
  }
  render() {
    return (
      <div className="detail-page" data-testid="pokemon-details-page">
        {this.state.pokemonId && this.state.pokemon ? (
          <>
            <div className="pokemon-img">
              <img
                alt={`sprite-${this.state.pokemon.name}`}
                className="detail-pokemon-image"
                src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${this.state.pokemonId}.png`}
              />
              <p className="capitalize">
                {this.state.pokemon.types.map((item, index) => {
                  return `${item.type.name} ${
                    index === this.state.pokemon.types.length - 1 ? "" : "/"
                  } `;
                })}
                pokemon
              </p>
            </div>
            <br></br>
            <div className="pokemon-data">
              <h3 className="capitalize">{this.state.pokemon.name}</h3>
              <b>Abilities:</b>
              {this.state.pokemon.abilities.map((item, index) => {
                return (
                  <p key={`ability-${index}`} className="capitalize">
                    {item.ability.name}
                  </p>
                );
              })}
              {this.state.caught ? (
                this.state.added ? (
                  <>
                    <p>
                      <i>Added! view your pokemon in your pokedex.</i>
                    </p>
                    <Link to="/pokedex/">View my pokemons</Link>
                  </>
                ) : (
                  <>
                    <p>Caught! Name your pokemon below :</p>
                    <form onSubmit={this.handleNicknameSubmit}>
                      <div className="caught-form">
                        <input
                          type="text"
                          className="name-input"
                          value={this.state.nickname}
                          onChange={this.handleNicknameInput}
                        />
                        <button
                          disabled={this.state.nickname === ""}
                          type="submit"
                          className="catch-button"
                        >
                          ADD POKEMON
                        </button>
                      </div>
                    </form>
                  </>
                )
              ) : (
                <button
                  className="catch-button"
                  onClick={() => {
                    this.tryToCatch();
                  }}
                >
                  CATCH!
                </button>
              )}
              {this.state.tried && !this.state.caught ? (
                <p>
                  <i> Missed! try again...</i>
                </p>
              ) : null}
            </div>
          </>
        ) : (
          <i>Loading Pokemon....</i>
        )}
      </div>
    );
  }
}

export default withRouter(Detail);
