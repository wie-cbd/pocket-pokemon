import React from "react";
import { withRouter } from "react-router-dom";
import { getPokedex, removePokemon } from "../services/PokeAPI";

class Pokedex extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pokedex: null
    };
  }
  componentDidMount() {
    this.setState({
      pokedex: getPokedex()
    });
  }
  deleteSelectedPokemon(index) {
    removePokemon(index);
    this.setState({ pokedex: getPokedex() });
  }
  render() {
    return (
      <div data-testid="pokedex-page">
        <div className="page-title">
          <h1>Pokedex</h1>
          <i>Pokemons You've caught</i>
        </div>
        {this.state.pokedex ? (
          this.state.pokedex.length > 0 ? (
            <div
              className={`pokecard-container ${
                this.state.pokedex.length < 3 ? "flex-justify-start " : null
              }`}
            >
              {this.state.pokedex.map((poke, index) => {
                return (
                  <div
                    className="poke-card my-pokemon"
                    key={`pokemon-${index}-${poke.name}`}
                  >
                    <img
                      alt={`sprite-${poke.name}`}
                      src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${poke.pokemonId}.png`}
                    />
                    <h3 className="capitalize">{poke.nickname}</h3>
                    <h4 className="capitalize">{poke.name}</h4>
                    <button
                      onClick={() => this.deleteSelectedPokemon(index)}
                      className="delete-button"
                    >
                      DELETE
                    </button>
                  </div>
                );
              })}
            </div>
          ) : (
            <div className="text-center">
              <p>You have no pokemons.</p>
            </div>
          )
        ) : (
          <i>Loading....</i>
        )}
      </div>
    );
  }
}

export default withRouter(Pokedex);
