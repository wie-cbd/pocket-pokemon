import axios from "axios";

const client = axios.create({
  baseURL: "https://pokeapi.co/api/v2/",
  timeout: 10000
});

export async function getPokemons() {
  const result = await client.get("/pokemon?offset0&limit=50");
  return result.data.results;
}

export async function getPokemonById(pokeId) {
  const result = await client.get(`/pokemon/${pokeId}`);
  return result.data;
}

export async function getPaginatedPokemons(page, pageLimit) {
  // max offset is 20.
  // we only show 20 per page
  const offset = page * pageLimit;
  const result = await client.get(
    `/pokemon?offset=${offset}&limit=${pageLimit}`
  );
  return result.data.results;
}

export function catchPokemon(pokemonId, name) {
  let pokemons = getPokedex();
  const caught = Math.random() < 0.5;
  if (caught) {
    pokemons.push({ pokemonId: pokemonId, name: name });
    localStorage.setItem("pokedex", JSON.stringify(pokemons));
    return true;
  } else {
    console.log("better luck next time!");
    return false;
  }
}

export function addPokemon(pokemonId, name, nickname) {
  let pokemons = getPokedex();
  pokemons.push({ pokemonId: pokemonId, name: name, nickname: nickname });
  localStorage.setItem("pokedex", JSON.stringify(pokemons));
}

export function getPokedex() {
  let pokedex = JSON.parse(localStorage.getItem("pokedex"));
  if (pokedex === null) {
    pokedex = [];
  }
  return pokedex;
}

export function hasPokemon(pokeId) {
  let pokedex = getPokedex();
  return pokedex.includes(pokeId);
}
export function removePokemon(index) {
  alert("deleting pokemon!");
  let pokedex = getPokedex();
  pokedex.splice(index, 1);
  localStorage.setItem("pokedex", JSON.stringify(pokedex));
}
