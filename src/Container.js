import React, { useState } from "react";
import "./App.css";
import { Route, Switch, Redirect, Link } from "react-router-dom";
import Detail from "./pages/Detail";
import List from "./pages/List";
import Pokedex from "./pages/Pokedex";
import Logo from "./assets/img/pokemon-logo.svg";
import Menu from "./assets/img/bars.svg";
import Close from "./assets/img/close.svg";

function Container() {
  const [menu, toggleMenu] = useState(false);
  if (localStorage.getItem("pokedex") === null) {
    localStorage.setItem("pokedex", "[]");
  }
  function toggleMobileMenu() {
    toggleMenu(!menu);
  }
  return (
    <>
      <div className="header">
        <div className="header-inner">
          <div className="flex-half">
            <Link to="/list">
              <img src={Logo} alt="pokemon-logo" className="header-logo" />
            </Link>
          </div>
          <div className="flex-half">
            <div className="header-nav">
              <div className="header-nav-link">
                <Link to="/pokedex">Pokedex</Link>
              </div>
              <div className="header-nav-link">
                <Link to="/list">Pokemons</Link>
              </div>
            </div>
            <div className="mobile-menu-toggler">
              <img
                src={menu ? Close : Menu}
                onClick={() => toggleMobileMenu()}
              />
            </div>
          </div>
        </div>
        <div className={`mobile-menu ${menu ? "active" : ""}`}>
          <div>
            <Link onClick={() => toggleMobileMenu()} to="/list">
              <h1>Pokemons</h1>
            </Link>
            <Link onClick={() => toggleMobileMenu()} to="/pokedex">
              <h1>My Pokemons</h1>
            </Link>
          </div>
        </div>
      </div>
      <div className="App">
        <div className="app-inner">
          <Switch>
            <Route path="/list" component={List} exact={true} />
            <Route path="/list/:number" component={List} exact={true} />
            <Route path="/detail/:pokeId" component={Detail} exact={true} />
            <Route path="/pokedex/" component={Pokedex} exact={true} />
            <Redirect to="/list" />
          </Switch>
        </div>
      </div>
      <footer className="footer">
        <div className="footer-inner">
          <p>Pokémon and Pokémon character names are trademarks of Nintendo.</p>
        </div>
      </footer>
    </>
  );
}

export default Container;
